#!/usr/bin/python3

from keycloak import KeycloakAdmin
# https://python-keycloak.readthedocs.io/en/latest/index.html
from collections import defaultdict
from os import getenv
from datetime import datetime
from requests import Session

session = Session()
session.headers.update({'Accept': 'application/json'})
backend_host = getenv('ONBOARDING_HOSTURL', 'http://localhost:8080/')

admin = KeycloakAdmin(
            server_url=getenv('ONBOARDING_KEYCLOAK_URL', 'http://localhost:8080/auth/'),
            username=getenv('ONBOARDING_KEYCLOAK_USERNAME', 'user'),
            password=getenv('ONBOARDING_KEYCLOAK_PASSWORD', 'password'),
            realm_name="oerworldmap",
            user_realm_name="master")

user_email = input()
countries = defaultdict(int)

try:
    while user_email:
        user_id = admin.get_user_id(user_email)
        #print(user_email)
        if not user_id:
            user_email = input()
            continue
        response = session.get(f"{backend_host}/resource/urn:uuid:{user_id}", headers={'Accept': 'application/json'})
        if response.status_code == 404:
            user_email = input()
            continue
        user_data = response.json()
        country = user_data['about'].get('location', [{}])[0].get('address', {}).get('addressCountry')
        #print(country)
        countries[country] += 1
        user_email = input()
except EOFError:
    pass

print('\n'.join("%s: %s" % line for line in sorted(countries.items(), key=lambda e: e[1], reverse=True)))
