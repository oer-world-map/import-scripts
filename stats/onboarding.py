#!/usr/bin/python3

from keycloak import KeycloakAdmin
# https://python-keycloak.readthedocs.io/en/latest/index.html
from collections import defaultdict
from os import getenv
from datetime import datetime

admin = KeycloakAdmin(
            server_url=getenv('ONBOARDING_KEYCLOAK_URL', 'http://localhost:8080/auth/'),
            username=getenv('ONBOARDING_KEYCLOAK_USERNAME', 'user'),
            password=getenv('ONBOARDING_KEYCLOAK_PASSWORD', 'password'),
            realm_name="oerworldmap",
            user_realm_name="master")

verified_users = admin.get_users({'emailVerified': True})

onboarded_users = 0
onboarded2_users = 0
created_since_launch = 0

for user in verified_users:
    try:
        if user.get('attributes', {}).get('onboarding_email_2_sent'):
            onboarded2_users += 1
            continue
        elif user.get('attributes', {}).get('onboarding_email_sent'):
            onboarded_users += 1
            continue
        if datetime.fromtimestamp(user['createdTimestamp']/1000) >= datetime(2024, 5, 1):
            created_since_launch += 1
    except KeyError:
        print(user)

print(f"Verified onboarded users: {onboarded_users}")
print(f"Verified onboarded 2 users: {onboarded2_users}")

print(f"New verfied users since Launch: {created_since_launch}")

#with open('onboarded_users.csv', 'w') as file_handle:
#    file_handle.write('\n'.join([user['email'] for user in onboarded_users]))
#
#print("Onboarded users written to 'onboarded_users.csv'")
