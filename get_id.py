#!/usr/bin/python3

import argparse
import json
from pprint import pprint
from pathlib import Path


def get_by_id(data, id):
    #print('get_by_id', id)
    for index, member in enumerate(data['member']):
        if member['@id'].startswith(id):
            # id can end with `.about`
            return member


def main(args):

    data = json.load(open(
        args.data_dir / "oer-worldmap-dump.json", encoding="utf8"
    ))

    for index, member in enumerate(data['member']):
        match = False
        if member['@id'].startswith(args.ID):
            match = True
            print('match @id')
        try:
            assert member['feature']['id'] == args.ID
        except (KeyError, AssertionError):
            pass
        else:
            match = True
            print('match feature/id', member['feature']['id'])
            #exit(0)
        try:
            assert member['feature']['@id'] == args.ID
        except (KeyError, AssertionError):
            pass
        else:
            match = True
            print('match feature/@id')
        for provides in member.get('provides', []):
            if provides['@id'] == args.ID:
                match = True
                print('match provides/*/@id')

        if match:
            print('Index', index)
            pprint(member, width=230)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')
    parser.add_argument('ID')
    parser.add_argument('--data-dir', '-d',
                        help='Data directory containing the JSON dump file.',
                        default='../dump/', type=Path)
    args = parser.parse_args()
    main(args)
