#!/usr/bin/python3
# https://python-keycloak.readthedocs.io/en/latest/index.html

from datetime import datetime, timedelta
from keycloak import KeycloakAdmin
from os import getenv
from time import time, sleep

admin = KeycloakAdmin(
            server_url=getenv('ONBOARDING_KEYCLOAK_URL', 'http://localhost:8080/auth/'),
            username=getenv('ONBOARDING_KEYCLOAK_USERNAME', 'user'),
            password=getenv('ONBOARDING_KEYCLOAK_PASSWORD', 'password'),
            realm_name="oerworldmap",
            user_realm_name="master")
users = admin.get_users({"emailVerified": False})

keep_from = datetime.now() - timedelta(days=3*30)
print('Delete unverified users older than {keep_from}')

for user in users:
    if user['emailVerified'] is True:
        print(f'Skip {user["email"]} because e-mail is verified')
        continue
    if 'profile_id' in user.get('attributes', {}):
        print(f'Skip {user["email"]} because profile_id set')
        continue
    created = datetime.fromtimestamp(user['createdTimestamp']/1000)
    print(created)
    if created > keep_from:
        print(f'User user["email"] is too young.')
        continue
    response = admin.delete_user(user_id=user['id'])
    if response != {}:
        print(f'Response is {response!r}')
        break
    print(f'Deleted user {user["id"]} with email {user["email"]}')
