from json import load as json_load
from urllib.parse import urlparse

FALLBACK_GENERIC_ABOUT = json_load(open('./conversion_helpers/default_about.json'))
FALLBACK_CROSS_SECTOR = [{'@id': 'https://oerworldmap.org/assets/json/sectors.json#general', '@type': 'Concept', 'name': {'de': 'Sektorenübergreifend', 'en': 'Cross-sector'}}],
FALLBACK_ADDITIONAL_TYPE = json_load(open('./conversion_helpers/default_additional_type.json'))


TLD_TO_CC = {
    'gov': 'US',
    'uk': 'GB',
    'jetzt': 'DE',
    'schule': 'DE',
    'education': 'US',
    'directory': 'US',
    'org': 'US',
    'eu': 'BE',
    'edu': 'US',
    'com': 'US',
}


def convert_display_name(resource_id, resource, get_resource):
    del resource['displayName']
    if 'countryChampionFor' in resource:
        del resource['countryChampionFor']
    if 'primarySector' not in resource:
        resource['primarySector'] = FALLBACK_CROSS_SECTOR
    elif len(resource['primarySector']) > 1:
        # urn:uuid:5d695f8a-bc88-4417-a754-ce868ecbf1b0 had two primary Sectors
        resource['primarySector'] = [resource['primarySector'][0]]
    if 'additionalType' not in resource:
        resource['additionalType'] = FALLBACK_ADDITIONAL_TYPE
    if 'url' not in resource:
        resource['url'] = 'https://example.invalid'
    if 'location' in resource and len(resource['location']) > 1:
        # resource urn:uuid:6a9a0e62-448d-40fb-8519-66cf05a9203b had two locations
        resource['location'] = [resource['location'][0]]
    if 'location' not in resource or not resource['location'][0]:
        tld = urlparse(resource['url']).hostname.split('.')[-1]
        cc = TLD_TO_CC.get(tld, tld).upper()
        resource['location'] = [{'address': {'addressCountry': cc}}]
    else:
        try:
            resource['location'][0]['address']['postalCode'] = str(resource['location'][0]['address']['postalCode'])
        except KeyError:
            pass
    for time_spec in ('startTime', 'endTime'):
        if time_spec not in resource:
            continue
        time = resource[time_spec]
        try:
            int(time)
        except ValueError:
            time_is_int = False
        else:
            time_is_int = True
        if time_is_int and len(time) == 8:
            resource[time_spec] = '%s-%s-%s' % (time[:4], time[4:6], time[6:])
    return resource
