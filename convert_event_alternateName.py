from re import compile as re_compile
from convert_display_name import FALLBACK_CROSS_SECTOR


RE_WHITESPACE = re_compile(r'\s+')


def convert_event_alternateName(resource_id, resource, get_resource):
    # special cases
    if 'url' not in resource and 'organizer' not in resource and resource_id == 'urn:uuid:805ab624-dcda-46c5-96a4-f31ec411468a':
        print("Missing URL, using https://cead.unb.br/")
        resource['url'] = 'https://cead.unb.br/'
    if 'location' not in resource and 'organizer' not in resource and resource_id == 'urn:uuid:f7ff6bd6-8086-44d9-9abb-f0a7d2bf11a8':
        print("Missing location, using https://cead.unb.br/")
        resource['location'] = [{'address': {'addressCountry': 'GB'}}]

    # normal processing
    if 'hashtag' not in resource:
        if 'en' in resource['alternateName']:
            resource['hashtag'] = '#' + RE_WHITESPACE.sub('_', resource['alternateName']['en'])
        else:
            resource['hashtag'] = '#' + RE_WHITESPACE.sub('_', resource['alternateName'][list(resource['alternateName'].keys())[0]])
    if 'inLanguage' not in resource:
        resource['inLanguage'] = list(resource['description'].keys())
        print("Missing language, using description's languages")
    if 'primarySector' not in resource:
        resource['primarySector'] = FALLBACK_CROSS_SECTOR
        print('Missing sector, using cross-sector.')
    if 'url' not in resource:
        resource['url'] = get_resource(resource['organizer'][0]['@id'])['about']['url']
        print("Missing URL, using organizer's")
    if 'location' not in resource or resource['location'] and resource['location'][0] == {}:
        resource['location'] = get_resource(resource['organizer'][0]['@id'])['about']['location']
        print("Missing location, using organizer's")

    if 'recordedIn' in resource:
        del resource['recordedIn']

    print('Deleting alternateName:', resource['alternateName'])
    del resource['alternateName']
    return resource
