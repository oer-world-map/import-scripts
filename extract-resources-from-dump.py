#!/usr/bin/python3

import argparse
from json import load
from pprint import pprint
from get_id import get_by_id


def get_name(name: dict) -> str:
    if isinstance(name, dict):
        if "name" in name:
            name = name['name']
        elif "name" in name.get('about', {}):
            name = name['about']['name']
    if not name:
        return "Unknown"
    if "en" in name:
        return name["en"]
    else:
        return name[list(name.keys())[0]]

parser = argparse.ArgumentParser(
    prog='ProgramName',
    description='What the program does',
    epilog='Text at the bottom of help')
parser.add_argument('json_dump')
parser.add_argument('resource_type')
parser.add_argument('output_file')
args = parser.parse_args()

data = load(open(args.json_dump))
with open(args.output_file, 'w') as output:
    output.write('<html><body>')
    for resource in filter(lambda x: x['about']['@type'] == args.resource_type, data['member']):
        names = []
        for lang, name in resource["about"]["name"].items():
            names.append(f'{lang.upper()}: {name}')
        output.write(f'<h2 id="{resource["@id"]}">' + '<br />'.join(names) + '</h2>')
        output.write(f'<dl><dt>Erstellt:</dt><dd>{resource["dateCreated"].replace("T", " ").replace("+", " +")}</dd>')
        if resource["dateCreated"] != resource["dateModified"]:
            output.write(f'<dt>Geändert:</dt><dd>{resource["dateModified"].replace("T", " ").replace("+", " +")}</dd>')
        if resource["author"]:
            output.write(f'<dt>Autor:</dt><dd><a href="https://oerworldmap.org/resource/{resource["author"]}">{get_name(get_by_id(data, resource["author"]))}</a></dd>')
        if resource["contributor"] and resource["contributor"] != resource["author"]:
            output.write(f'<dt>contributor:</dt><dd><a href="https://oerworldmap.org/resource/{resource["contributor"]}">{get_name(get_by_id(data, resource["contributor"]))}</a></dd>')
        if "url" in resource["about"]:
            output.write(f'<dt>URL:</dt><dd><a href="{resource["about"]["url"]}">{resource["about"]["url"]}</a></dd>')
        if "email" in resource["about"]:
            output.write(f'<dt>E-Mail:</dt><dd><a href="mailto:{resource["about"]["email"]}">{resource["about"]["email"]}</a></dd>')
        if "isPartOf" in resource["about"]:
            output.write(f'<dt>isPartOf:</dt><dd><a href="https://oerworldmap.org/resource/{resource["about"]["isPartOf"]["@id"]}">{get_name(resource["about"]["isPartOf"]["name"])}</a></dd>')
        if int(resource["like_count"]):
            output.write(f'<dt>Likes:</dt><dd>{resource["like_count"]}</dd>')
        if int(resource["lighthouse_count"]):
            output.write(f'<dt>Lighthouses:</dt><dd>{resource["lighthouse_count"]}</dd>')
        if "primarySector" in resource["about"]:
            output.write(f'<dt>Sektor:</dt><dd>{resource["about"]["primarySector"][0]["name"]["en"]}</dd>')
        if "secondarySector" in resource["about"]:
            output.write(f'<dt>Sektor:</dt><dd>{resource["about"]["secondarySector"][0]["name"]["en"]}</dd>')
        if "startTime" in resource["about"]:
            output.write(f'<dt>Start:</dt><dd>{resource["about"]["startTime"].replace("T", " ").replace("+", " +")}</dd>')
        if "endTime" in resource["about"]:
            output.write(f'<dt>Ende:</dt><dd>{resource["about"]["endTime"].replace("T", " ").replace("+", " +")}</dd>')
        if "datePublished" in resource["about"]:
            output.write(f'<dt>Veröffentlichungsdatum:</dt><dd>{resource["about"]["datePublished"].replace("T", " ").replace("+", " +")}</dd>')
        if "citation" in resource["about"]:
            output.write(f'<dt>Zitate:</dt><dd>{resource["about"]["citation"]}</dd>')
        if "inLanguage" in resource["about"]:
            output.write(f'<dt>Zitate:</dt><dd>{", ".join(resource["about"]["inLanguage"])}</dd>')
        if "location" in resource["about"] and resource["about"]["location"][0]:
            address = []
            for detail in ("streetAddress", "postalCode", "addressLocality", "addressRegion", "addressCountry"):
                if detail in resource["about"]["location"][0]["address"]:
                    address.append(resource["about"]["location"][0]["address"][detail])
            output.write(f'<dt>Adresse:</dt><dd>{", ".join(address)}</dd>')
            if "geo" in resource["about"]["location"][0]:
                output.write(f'<dt>Koordinaten:</dt><dd>{resource["about"]["location"][0]["geo"]["lat"]} {resource["about"]["location"][0]["geo"]["lon"]}</dd>')
        if "image" in resource["about"]:
            output.write(f'<dt>Bild:</dt><dd><img src="{resource["about"]["image"]}" /></dd>')
        output.write("</dl>")

        for key in ("agent", "isBasedOn", "isBasisFor", "affiliate", "memberOf", "participant", "hasPart", "organizerFor", "result", "mentions", "publisher", "license", "creator"):
            if key in resource["about"]:
                output.write(f"{key.title()}:<ul>")
                for value in resource["about"][key]:
                    output.write(f'<li><a href="https://oerworldmap.org/resource/{value["@id"]}">{get_name(value["name"]) if "name" in value else "unknown name"}</a></li>')
                output.write("</ul>")
        if "keywords" in resource["about"]:
            output.write("Keywords:<ul>")
            for keyword in resource["about"]["keywords"]:
                output.write(f'<li>{keyword}</li>')
            output.write("</ul>")
        for urllist in ("sameAs", "award"):
            if urllist in resource["about"]:
                output.write(f"{urllist.title()}:<ul>")
                for url in resource["about"][urllist]:
                    output.write(f'<li><a href="{url}">{url}</a></li>')
                output.write("</ul>")
        if "isFundedBy" in resource["about"]:
            output.write('<h3>Funded By</h3>')
            for funded in resource["about"]["isFundedBy"]:
                if "@type" not in funded:
                    continue
                output.write(f'<dl><dt>Typ:</dt><dd>{funded["@type"]}</dd>')
                if "hasMonetaryValue" in funded:
                    output.write(f'<dt>Monetary Value:</dt><dd>{funded["hasMonetaryValue"]}</dd>')
                output.write(f'</dl>Awarded by:<ul>')
                for awardedBy in funded.get('isAwardedBy', []):
                    output.write(f'<li>Awarded by: <a href="{awardedBy["@id"]}">{get_name(awardedBy["name"])}</a></li>')
                output.write('</ul>')
                if set(funded.keys()) - set(('isAwardedBy', '@type', 'funds', 'sameAs', 'hasMonetaryValue')):
                    pprint(resource["about"]["isFundedBy"])

        if "description" in resource["about"]:
            output.write("<h3>Beschreibung</h3>")
            for lang in resource["about"]["description"]:
                # Fix broken markup in resource "tutory BETA"
                text = resource["about"]["description"][lang].replace("<b>\r\n", "</b>\r\n")
                output.write(f'<h4>{lang}</h4><p>{text}</p>')
        if "spatial" in resource["about"]:
            output.write("<h3>Spatial</h3>")
            for lang in resource["about"]["spatial"]:
                output.write(f'<h4>{lang}</h4><p>{resource["about"]["spatial"][lang]}</p>')
        if "comment" in resource["about"]:
            output.write("<h3>Kommentare</h3>")
            for comment in resource["about"]["comment"]:
                if "author" in comment:
                    output.write(f'Autor: <a href="https://oerworldmap.org/resource/{comment["author"][0]}">Autor</a>')
                for lang in comment.get("text", []):
                    output.write(f'<h4>{lang}</h4><p>{comment["text"][lang]}</p>')

        about_diff = set(resource['about'].keys()) - set(('name', 'url', '@type', 'email', '@id', 'additionalType', 'primarySector', 'secondarySector', '@context', 'startTime', 'endTime', 'image', 'isFundedBy', 'alternateName', 'mentionedIn', 'agent', 'displayName', 'objectIn', 'description', 'comment', 'location', 'keywords', 'sameAs', 'affiliate', 'publication', 'isPartOf', 'isBasedOn', 'isBasisFor', 'memberOf', 'participant', 'hasPart', 'organizerFor', 'award', 'result', 'publisher', 'datePublished', 'citation', 'license', 'inLanguage', 'mentions', 'creator', 'spatial'))
        if about_diff:
            print('About Diff:', about_diff)
            pprint(resource['about'])
            break
        key_diff = set(resource.keys()) - set(('about', 'dateCreated', '@id', '@type', '@context', 'author', 'contributor', 'like_count', 'lighthouse_count', 'link_count', 'dateModified', 'feature'))
        if key_diff:
            print('Key Diff:', key_diff)
            break
    output.write('</html></body>')
