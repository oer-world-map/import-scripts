#!/usr/bin/python3

import json
import requests
import pprint


IGNORE_FIELDS = ('like_count',
                 'link_count',
                 'lighthouse_count',
                 'dateCreated',
                 'dateModified')
submit = False


if __name__ == '__main__':
    data = json.load(open(
        "/home/intevation/dump/oer-worldmap-dump.json", encoding="utf8"
    ))


    for index, member in enumerate(data['member']):
        print('###', index)
        payload = member.copy()
        for field in IGNORE_FIELDS:
            del payload[field]

        pprint.pprint(payload, width=230)
        response = requests.post("http://scala:9000/resource/", json=payload)
        print(response)
        try:
            pprint.pprint(response.json(), width=230)
        except requests.exceptions.JSONDecodeError:
            pprint.pprint(response.text, width=230)
        response.raise_for_status()
        break
