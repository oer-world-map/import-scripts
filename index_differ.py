#!/usr/bin/python3

import json

schema = json.load(open('../oerworldmap/public/json/schema.json'))
index = json.load(open('../oerworldmap/conf/index-config.json'))

schema_fields = set()
for resource_type in schema['oneOf']:
    resource_type = resource_type['$ref'][resource_type['$ref'].rfind('/')+1:]
    schema_fields |= schema['definitions'][resource_type]['properties'].keys()
schema_fields |= schema['definitions']['Comment']['properties'].keys()

index_fields = set(index['mappings']['WebPage']['properties']['about']['properties'].keys())

print(f'In schema but not in index: {schema_fields - index_fields}')
print(f'In index but not in schema: {index_fields - schema_fields}')
