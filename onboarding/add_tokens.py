#!/usr/bin/python3

from codecs import open as codecs_open
from csv import DictWriter
from uuid import uuid4
from requests import Session

KEYCLOAK_URL = "http://localhost:9001/auth"

s = Session()
r = s.post(f"{KEYCLOAK_URL}/realms/master/protocol/openid-connect/token", data={"username": "user", "password": "password", "grant_type": 'password', "client_id": 'admin-cli'})
r.raise_for_status()

token = r.json()['access_token']
s.headers.update({'Authorization': f'Bearer {token}'})

r = s.get(f"{KEYCLOAK_URL}/admin/realms/oerworldmap/users?briefRepresentation=true&emailVerified=false&max=100000")
r.raise_for_status()
users = r.json()
print(f"Retrieved {len(users)} users")

existing_token = 0
added_token = 0
skipped = 0
user_list = []
for user in users:
    user_id = user['id']
    r = s.get(f"{KEYCLOAK_URL}/admin/realms/oerworldmap/users/{user_id}")
    r.raise_for_status()
    user_data = r.json()
    if 'attributes' not in user_data:
        user_data['attributes'] = {}
    if 'onboarding_token' in user_data['attributes']:
        #print(f'Skip {user["email"]} because has no onboarding_token')
        skipped += 1
        continue
    if 'profile_id' not in user_data['attributes']:
        print(f'Skip {user["email"]} because has no profile_id')
        skipped += 1
        continue
    if 'onboarding_token' in user_data['attributes']:
        existing_token += 1
    else:
        user_data['attributes']['onboarding_token'] = [str(uuid4())]
        r = s.put(f"{KEYCLOAK_URL}/admin/realms/oerworldmap/users/{user_id}", json=user_data)
        r.raise_for_status()
        added_token += 1
    user_list.append({'email': user_data['email'],
                      'firstname': user_data.get('firstName', ''),
                      'lastname': user_data.get('lastName', ''),
                      'token': user_data['attributes']['onboarding_token'][0]})

print(f"Added tokens to {added_token} users, {existing_token} users already had a token")

with codecs_open('user_onboarding_data.csv', 'w+', encoding='utf8') as fhandle:
    writer = DictWriter(fhandle, fieldnames=user_list[0].keys())
    writer.writerows(user_list)
