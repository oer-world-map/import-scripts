#!/usr/bin/python3
# https://python-keycloak.readthedocs.io/en/latest/index.html

from keycloak import KeycloakAdmin
from email.message import EmailMessage
from smtplib import SMTP_SSL
from urllib.parse import quote
from os import getenv
from time import time, sleep
from imaplib import IMAP4_SSL, Time2Internaldate

SENDER = getenv('ONBOARDING_FROM', 'support@oerworldmap.org')
HOSTURL = getenv('ONBOARDING_HOSTURL', 'http://localhost:8080')

with open('template.txt') as fp:
    TEXT = fp.read()
with open('template.html') as fp:
    HTML = fp.read()
try:
    with open('exemptions.txt') as fp:
        exemptions = {line.strip() for line in fp.readlines()}
        print(f'Loaded {len(exemptions)} exemptions.')
except FileNotFoundError:
    exemptions = set()

admin = KeycloakAdmin(
            server_url=getenv('ONBOARDING_KEYCLOAK_URL', 'http://localhost:8080/auth/'),
            username=getenv('ONBOARDING_KEYCLOAK_USERNAME', 'user'),
            password=getenv('ONBOARDING_KEYCLOAK_PASSWORD', 'password'),
            realm_name="oerworldmap",
            user_realm_name="master")
if getenv('ONBOARDING_IMAP_SERVER'):
    imap = IMAP4_SSL(getenv('ONBOARDING_IMAP_SERVER', 'localhost'), 993)
    imap.login(getenv('ONBOARDING_IMAP_USERNAME', 'localhost'), getenv('ONBOARDING_IMAP_PASSWORD', 'localhost'))
#smtp = SMTP_SSL(getenv('ONBOARDING_SMTP_SERVER', 'localhost'))
if getenv('ONBOARDING_SMTP_USERNAME'):
    smtp.login(getenv('ONBOARDING_SMTP_USERNAME'), getenv('ONBOARDING_SMTP_PASSWORD'))

users = admin.get_users()

for user in users:
    user_email = user['email']
    if user_email in exemptions:
        print('Skip user because e-mail is in exemptions')
        continue
    if 'onboarding_token' not in user.get('attributes', {}):
        #print(f'Skip {user_email} because has no onboarding_token')
        continue
    if 'onboarding_email_sent' not in user.get('attributes', {}):
        #print(f'Skip {user_email} because onboarding_email_2_sent')
        continue
    if 'onboarding_email_2_sent' in user.get('attributes', {}):
        #print(f'Skip {user_email} because onboarding_email_2_sent')
        continue
    if user['emailVerified'] is True:
        print(f'Skip {user_email} because e-mail is verified')
        continue
    if 'profile_id' not in user.get('attributes', {}):
        #print(f'Skip {user_email} because profile_id not set')
        continue
    message = EmailMessage()
    message.add_header('Auto-Submitted', 'auto-generated')
    message.add_header('X-Auto-Response-Suppress', 'OOF')
    message.add_header('Reply-To', f'OER World Map <{SENDER}>')

    message.set_content(TEXT.format(token=quote(user['attributes']['onboarding_token'][0].encode()),
                                    username=quote(user_email.encode()),
                                    hosturl=HOSTURL))
    message.add_alternative(HTML.format(token=quote(user['attributes']['onboarding_token'][0].encode()),
                                        username=quote(user_email.encode()),
                                        hosturl=HOSTURL), subtype='html')

    message['Subject'] = f'OER World Map is back online'
    message['From'] = SENDER
    message['To'] = user_email
    #smtp.send_message(message)
    user['attributes']['onboarding_email_2_sent'] = 'true'
    response = admin.update_user(user_id=user['id'], payload=user)
    if response != {}:
        print(f'Response is {response!r}')
        break
    if getenv('ONBOARDING_IMAP_SERVER'):
        imap.append('INBOX.Sent', '\\Seen', Time2Internaldate(time()), message.as_string().encode('utf8'))
    print(f'Sent to {user_email}')
    #sleep(15)

#smtp.quit()
if getenv('ONBOARDING_IMAP_SERVER'):
    imap.logout()
