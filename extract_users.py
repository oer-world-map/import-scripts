#!/usr/bin/python3

from datetime import datetime
from json import dump as json_dump
from sys import stderr

from rdflib import Graph, Literal, RDF, URIRef
from rdflib.term import URIRef
from rdflib.namespace import FOAF, XSD

g = Graph()
g.parse('dump')

Type = URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
Person = URIRef('http://schema.org/Person')
Name = URIRef('http://schema.org/name')
Email = URIRef('http://schema.org/email')

DATA = {
  "realm" : "oerworldmap",
  "users" : []
}


def generate_user(user_id: str, name: str, email: str = None) -> dict:
    return {
    "id" : str(user_id)[9:],
    "createdTimestamp" : int(datetime.now().timestamp()*1000),
    "username" : email,
    "enabled" : True,
    "totp" : False,
#    "emailVerified" : True,
    "firstName" : ' '.join(name.split(' ')[:-1]).strip(),
    "lastName" : name.split(' ')[-1].strip(),
    "email" : email,
    "attributes" : {
      "profile_id" : [str(user_id)]
    },
    "credentials" : [],
    "disableableCredentialTypes" : [ ],
    "requiredActions" : ["VERIFY_EMAIL", "UPDATE_PASSWORD"],
    "realmRoles" : ["default-roles-oerworldmap"],
    "notBefore" : 0,
    "groups" : []
  }

all_emails = set()

for person in g.subjects(Type, Person):
    names = list(g.objects(person, Name))
    if len(names) == 1:
        en_name = names[0]
    else:
        for name in names:
            if name.language == 'en':
                en_name = name
                break
        else:
            en_name = names[0]
            print(f'{id} has multiple names, but none is English: {names!r}. Using the first one.', file=stderr)

    emails = list(map(str.lower, g.objects(person, Email)))
    if not emails:
        continue

    if emails[0] in all_emails:
        print(f'{person} has a duplicate email address: {emails}. Skipping this entry.')
        continue
    all_emails.add(emails[0])

    if len(emails) > 1:
        print(f'{id} has multiple email addresses: {names!r}. Using the first one.', file=stderr)
    DATA['users'].append(generate_user(user_id=person, name=en_name, email=emails[0]))

with open('keycloak_users.json', 'w') as output:
    json_dump(obj=DATA, fp=output, indent=2, separators=(',', ': '))

print(f"Exported {len(DATA['users'])} user accounts.")
