# OER World Map Import Tools

Import Tool and Query Tools for the OER World Map dump at https://zenodo.org/records/6922065

## Pre-Processing

To fix obviously invalid URL syntax in the data dump ([see issue tracker](https://gitlab.com/oer-world-map/import-scripts/-/issues/?label_name[]=data&state=closed)), run this command first on your dump files (depends on which dump you are going to import):
```bash
sed -i -r -e 's#https?://(https?://)#\1#' -e 's#http(s?):([^/])#http\1://\2#g' -e 's#https: //#https://#g' oer-worldmap-dump.csv oer-worldmap-dump.json commits/objects/*
```

## Scripts

- `import_json`: Import the JSON-Dump into the Application (does not work).
- `import_csv_app.py`: Import the CSV-Dump (enriched by information from the JSON dump) into the app.
- `import_json_es.py`: Import the JSON-dump into Elasticsearch directly. Data will be only in the search, not the application itself.

### Utilities

Used by the scripts and as command line tool

- `get_id`: Get related entries from the dump by UUID
- `get_types`: Get all types from the schemas

### Usage

See the help pages (`-h` / `--help`) of the programs themselves.

#### Example: Get all tags (keywords) sorted by count

```bash
./editor.py -H 'https://oerworldmap.org' -e about.keywords -S > keywords.json
tail -n +2 keywords.json | jq -r '.["about.keywords"].[]' < keywords.json | sort | uniq -c | sort -nr
```

## Import from history objects

```bash
# stop backend
rm -rf data/commits/*
rm -rf data/tdb/*
cp $source/commits/* data/commits/*
# start the backend
```

The import can take a while.
Afterwards there is an index operation, which takes even longer. This also writes the ES data.

The import and index operations are only executed if there is an empty/no tdb database.

## Migrate ES dump

https://www.npmjs.com/package/elasticdump

* create the index as documented in the README of the main repository
* `elasticdump --input=oerworldmap.json --output=http://localhost:9200/oerworldmap --type=data`

## Users

To create user accounts in Keycloak, follow these steps:

### Dump RDF data

Download the [Apache Jena TDB command line tools](https://jena.apache.org/documentation/tdb2/tdb2_cmds.html) from https://jena.apache.org/download/index.cgi

```bash
tdb2.tdbdump --loc path/to/tdb2/database > dump
```

Map a directory into the container to transfer the data:
```
     volumes:
       - keycloak:/opt/keycloak/data
+      - ../data/keycloak_data:/opt/keycloak_data:rw
```
And re-start the container

Enter the keycloak container and export the realm to the location accessible (e.g. a docker volume) to the outside:
```bash
docker exec -ti docker_keycloak_1 bash
/opt/keycloak/bin/kc.sh export --dir /opt/keycloak_data/ --users different_files --realm oerworldmap --users-per-file 10000
```

Run the user export scripts, this generates the user data for Keycloak from the data dump into `keycloak_users.json`:
```bash
./extract_users.py
./extract_users_nested.py
```
It is recommended to run them in two different rounds to minimize the chance of duplicates.

Transfer back the users to the container:
```bash
cp keycloak_users.json > ../oerworldmap/data/keycloak_data/oerworldmap-users-[nextfreenumber].json
cp keycloak_users_nested.json > ../oerworldmap/data/keycloak_data/oerworldmap-users-[nextfreenumber].json
```

Then import the realm (overwrites and re-creates the realm) in the keycloak container:
```bash
/opt/keycloak/bin/kc.sh import --dir /opt/keycloak_data/ && echo "Realm successfully imported!"
```

# Extract data from dump

To extract all resources of a specific type from a JSON dump to human-readable HTML, use the script `extract-resources-from-dump.py`.

Example:
```
./extract-resources-from-dump.py dump/oer-worldmap-dump.json WebPage publication.html
```

Works for these types:
- WebPage (Publications)
- Article (Stories)
- Action (Projects)

Other types will need more mappings
