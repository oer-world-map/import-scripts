#!/usr/bin/python3

import argparse
import csv
import codecs
import re
import requests

from copy import deepcopy
from pprint import pprint as pprint_orig
from typing import Optional
from pathlib import Path
from json import load as json_load

from get_types import TYPES_MAP
from get_id import get_by_id
from import_json import IGNORE_FIELDS


def pprint(*args, **kwargs):
    pprint_orig(*args, width=230, **kwargs)


EASYFIELDS = ['startTime', 'endTime',
              'startDate', 'endDate',
              'url',
              '@type', '@id']

AGENT_MAP = {}
RE_DATE = re.compile('^([0-9]{4})([0-9]{2})([0-9]{2})$')
RE_DATE_NOT_3_MS = re.compile(r'^([0-9-]{10}T[0-9:]{8}\.)([0-9]{1,2}|[0-9]{4})([^0-9].*?)$')
LICENSE_UNKNOWN = {
    "image": "https://cdn0.iconfinder.com/data/icons/glyphpack/34/question-circle-64.png",
    "@type": "Concept",
    "name": {
      "de": "nicht spezifiziert",
      "pt": "Não especificado",
      "en": "Unspecified"
    },
    "@id": "https://oerworldmap.org/assets/json/licenses.json#unspecified"
  }


def main(data_dir: Path, start_index: Optional[str] = None, verbose: bool = False, single: bool = False,
         only_types: Optional[list] = None, excluded_types: Optional[list] = None,
         dry_run: bool = False, end_index: Optional[str] = None):
    # if start_index is given, do not submit from the start
    submit = not start_index

    json_data = json_load(open(
        data_dir / "oer-worldmap-dump.json", encoding="utf8"
    ))

    with codecs.open(
        data_dir / "oer-worldmap-dump.csv", encoding="utf8"
    ) as filehandle:
        reader = csv.DictReader(filehandle)
        for row in reader:
            if row['/@type'] in ('Person', 'Organization'):
                AGENT_MAP[row['/name/en']] = row['/@id']
        total_lines = reader.line_num
        filehandle.seek(0)

        for index, row in enumerate(reader):
            if row["/@type"] == '/@type':
                # header
                continue
            if start_index and row["/@id"] == start_index:
                print(f"start at index {index}")
                submit = True
            if end_index and row["/@id"] == end_index:
                print('Last allowed index reached, stopping')
                break
            if only_types and row['/@type'] not in only_types:
                if verbose:
                    print(f"Skip row of type {row['/@type']!r}, not in list of allowed types.")
                continue
            if excluded_types and row['/@type'] in excluded_types:
                if verbose:
                    print(f"Skip row of type {row['/@type']!r}, in list of disallowed types.")
                continue

            stop = False
            payload = {
                "@type": row["/@type"],
                "name": {},
                "description": {},
                "location": [],
                "agent": [],
                "provider": [],
                "additionalType": [],
                "primarySector": [],
            }
            for key, value in row.items():

                if value == "":
                    continue

                if key[1:] in EASYFIELDS:
                    payload[key[1:]] = value
                elif key.startswith("/name/"):
                    lang = key[6:]
                    payload["name"][lang] = value
                elif key.startswith('/location'):
                    number = int(key.split('/')[2])
                    assert key.split('/')[3] == 'address'
                    field2 = key.split('/')[4]
                    try:
                        payload["location"][number]["address"][field2] = value
                    except IndexError:
                        payload["location"].append({"address": {field2: value}})
                elif key.startswith("/agent/") or key.startswith('/provider/') or key.startswith('/additionalType') or key.startswith('/primarySector'):
                    # only '/#field#/#number#/name/en' supported and relevant
                    field = key.split('/')[1]
                    number = int(key.split('/')[2])
                    assert key.split('/')[3] == 'name'
                    assert key.split('/')[4] == 'en'
                    try:
                        payload[field][number] = {"name": {"en": value}}
                    except IndexError:
                        payload[field].append({"name": {"en": value}})
                    if field in ('additionalType', 'primarySector'):
                        if value == 'Call for tender':  # Fix wrong naming
                            value = 'Call for proposals/tenders/grants'
                        try:
                            payload[field][-1]['@id'] = TYPES_MAP[value]
                        except KeyError:
                            print('Missing type map', row["/@type"], field, value, row)
                            stop = True
                            break
                    if field == 'agent':
                        payload[field][-1]['@id'] = AGENT_MAP[value]
                elif key.startswith("/description/"):
                    lang = key[13:]
                    payload["description"][lang] = value
                elif key == '/availableChannel/0/serviceUrl':
                    payload['availableChannel'] = [{'serviceUrl': value}]
                else:
                    print(f"Unable to map: {key} {value}")
                    stop = True
                    break

            if not submit:
                continue
            if verbose:
                print(f'INDEX {index}/{total_lines}')

            # delete empty lists and empty dicts
            for key in list(payload.keys()):
                if len(payload[key]) == 0:
                    del payload[key]

            #print('data before')
            #pprint(payload)
            json = get_by_id(json_data, row['/@id'])
            if row['/@type'] in ('Article', 'Collection', 'Person', 'Event', 'Action', 'Service'):
                for key, value in json['about'].items():
                    if key == '@type' or not len(value):  # ignore type and empty value
                        continue
                    if isinstance(value, list) and not value[0]:  # ignore if is a list with an empty value, e.g. 'location': [{}]
                        continue
                    payload[key] = value
            #print(0, payload.get('location'))
            # In Collection dateCreated is not allowed: https://gitlab.com/oer-world-map/import-scripts/-/issues/3
            if row['/@type'] in ('Article'):
                for json_field in ('dateCreated', ):
                    try:
                        payload[json_field] = json[json_field]
                    except KeyError:
                        pass
            # These fields are not allowed: https://gitlab.com/oer-world-map/import-scripts/-/issues/3
            #if row['/@type'] in ('Collection', ):
            #    for json_field in ('author', 'contributor', 'dateModified'):
            #        try:
            #            payload[json_field] = json[json_field].copy()
            #        except KeyError:
            #            pass
            #        except AttributeError: # type str has no copy, https://gitlab.com/oer-world-map/import-scripts/-/issues/2
            #            payload[json_field] = json[json_field]
            #print(2, payload.get('location'))
            try:
                for key, value in json['feature']['properties'].items():
                    if key == 'location' and value and isinstance(value, list) and value[0]:  # can be a list of dicts, ignore an empty dict
                        payload[key] = value
                    elif key == 'location' and value and isinstance(value, dict):  # or a dict
                        payload[key] = [value]
                    elif value:
                        payload[key] = value
            except KeyError:  # no feature
                pass
            if row['/@type'] in ('Article', 'WebPage'):
                if 'license' not in payload:
                    payload['license'] = LICENSE_UNKNOWN
            if row['/@type'] in ('Article', ):
                if 'url' in payload:
                    del payload['url']
            if row['/@type'] in ('Article', 'WebPage', 'Service'):
                if 'description' not in payload:
                    payload['description'] = {'en': 'No description available'}
            # location not allowed for Products https://gitlab.com/oer-world-map/import-scripts/-/issues/4
            if row['/@type'] in ('Product', ):
                if 'location' in payload:
                    del payload['location']
            #print(3, payload.get('location'))
            if 'location' in payload and len(payload['location']) > 1:
                # only keep the first location, only one is allowed
                # first filter out all empty dicts
                payload['location'] = list(filter(len, payload['location']))
                #print(4, payload['location'])
                # then keep the next location
                new_location = payload['location'][0]
                payload['location'] = [new_location]

            # Convert postalCode from int or float to string, without decimals
            # https://gitlab.com/oer-world-map/import-scripts/-/issues/13
            try:
                postalCode = payload['location'][0]['address']['postalCode']
                if not isinstance(postalCode, str):
                    payload['location'][0]['address']['postalCode'] = str(int(postalCode))
            except KeyError:
                pass

            #print(5, payload.get('location'))
            # /isFundedBy must have not more then one element
            if 'isFundedBy' in payload and len(payload['isFundedBy']) > 1:
                # filter out all empty elements, then keep the first
                payload['isFundedBy'] = list(filter(len, payload['isFundedBy']))
                new_value = payload['isFundedBy'][0]
                payload['isFundedBy'] = [new_value]

            # only one primarySector allowed (although it's a list...)
            if row["/@type"] in ('Action', 'Person') and 'primarySector' in payload and len(payload['primarySector']) == 2:
                del payload['primarySector'][1]
            if row["/@type"] == 'Action':
                for field in ("agent","endTime","startTime"):
                    try:
                        del payload[field]
                    except KeyError:
                        pass

            if row['/@type'] == 'Event':
                try:
                    payload['location'] = json['about']['organizer'][0]['location']
                except KeyError:
                    pass

            # if entry has no location, add a fake one if location is required
            if row["/@type"] in ('Person', 'Organization', 'Event') and 'location' not in payload:
                payload['location'] = [{'address': {'addressCountry': 'VA', 'addressRegion': 'Unknown', 'addressLocality': 'Address faked to make entry compatible with schema'}}]

            # only 'WebPage' is a valid type of a 'isBasedOn' field in entries of type 'Service'
            if row['/@type'] == 'Service' and 'isBasedOn' in payload:
                payload['isBasedOn'] = list(filter(lambda x: x['@type'] == 'WebPage', payload['isBasedOn']))
            # and provider too
            if row['/@type'] == 'Service' and 'provider' in payload:
                payload['provider'] = list(filter(lambda x: x['@type'] == 'WebPage', payload['provider']))
            # Language 'in' is not valid (/availableChannel/0/availableLanguage/15)
            if row['/@type'] == 'Service':
                for channel_index, channel in enumerate(payload['availableChannel']):
                    languages = payload['availableChannel'][channel_index].get('availableLanguage', ())
                    languages = list(filter(lambda x: x != 'in', languages))
                    languages = list(map(lambda x: 'pt' if x in ('pt-br', 'por') else x, languages))
                    if languages:
                        payload['availableChannel'][channel_index]['availableLanguage'] = languages
                # must have at least 'location' or 'provider'. payload['provider'] can be an empty list, equal to non-existence
                if ('provider' not in payload or not payload.get('provider')) and 'location' not in payload:
                    payload['location'] = [{'address': {'addressCountry': 'VA', 'addressRegion': 'Unknown', 'addressLocality': 'Address faked to make entry compatible with schema'}}]

            # Convert date format 20180430 to 2018-04-30
            for field in ('startTime', 'endTime', 'startDate'):
                try:
                    groups = RE_DATE.match(payload[field]).groups()
                except (AttributeError, KeyError):  # AttributeError if pattern did not match -> no groups
                    pass
                else:
                    payload[field] = f'{groups[0]}-{groups[1]}-{groups[2]}'
            # Convert date format with 1,2 or 4 microsecond digits to format with three microsecond digits
            for field in ('dateCreated', ):
                try:
                    groups = RE_DATE_NOT_3_MS.match(payload[field]).groups()
                except (AttributeError, KeyError):
                    pass
                else:
                    ms = (groups[1] + '000')[:3]
                    payload[field] = f'{groups[0]}{ms}{groups[2]}'

            # missing country codes for some entries
            if payload["@id"] == "urn:uuid:9f2bf35e-18e0-4254-9110-4576ce75ab85":
                payload['location'][0]['address']['addressCountry'] = 'GR'
            elif payload["@id"] in ("urn:uuid:3b2c9007-1330-4f92-8472-91131eaceb33", "urn:uuid:1d157596-e136-429d-8554-9f44c9cc1ffe", "urn:uuid:2cbe1850-c788-4b57-b7c0-6a9859b7528b", "urn:uuid:ef252eed-0094-4cad-9b80-3572c6fc3f03", "urn:uuid:a93d0b58-6dbb-49f9-b262-8252c52cc32b"):
                payload['location'][0]['address']['addressCountry'] = 'DE'

            if not submit:
                if verbose:
                    print("skip")
                continue
            print('Payload (Request):', end=' ')
            if verbose:
                pprint(payload)
            else:
                print(f"Type: {payload['@type']!r} Name: {payload['name']!r}")
            if stop:
                print("break")
                break

            if payload['@type'] == 'WebPage' and 'url' not in payload:
                # https://gitlab.com/oer-world-map/import-scripts/-/issues/1
                print('Error: skip submission because of missing URL, see #1')
                continue

            if dry_run:
                if verbose:
                    print('Dry run: Skip submission to application.')
                if single:
                    break
                continue

            response = requests.post("http://localhost:9000/resource/", json=payload)
            if verbose or not response.ok:
                print('Response:', response)
                try:
                    try:
                        pprint(list(filter(lambda item: item['domain'] != 'syntax', response.json())))
                    except (KeyError, TypeError):
                        pprint(response.json())
                except requests.exceptions.JSONDecodeError:
                    pprint(response.text)
            response.raise_for_status()

            if single:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                    prog='import_csv_app',
                    description='Imports the CSV data into the app',
                    epilog='Data linking not supported, but the data will be saved in the Apache Jena TDB Database')
    parser.add_argument('--data-dir', '-d',
                        help='Data directory containing the JSON and CSV dump files.',
                        default='../dump/', type=Path)
    parser.add_argument('start_index', nargs='?')
    parser.add_argument('end_index', nargs='?', help='Last index to process, exclusive.')
    parser.add_argument('--verbose', '-v', action='store_true')
    parser.add_argument('--single', '-s', action='store_true', help='Only import a single entry')
    parser.add_argument('--type', '-t', nargs='+', help='Only import these types')
    parser.add_argument('--not-type', '-T', nargs='+', help='Do not import these types')
    parser.add_argument('--dry-run', '-n', action='store_true')
    args = parser.parse_args()
    if args.verbose:
        print(f'Parsed Arguments: {args!r}')
    try:
        main(start_index=args.start_index, verbose=args.verbose, single=args.single,
             only_types=args.type, excluded_types=args.not_type, dry_run=args.dry_run,
         data_dir=args.data_dir, end_index=args.end_index)
    except KeyboardInterrupt:
        print("Interrupted")
