#!/usr/bin/python3

from csv import DictWriter
from json import load as json_load

filename_mapping = {
    'activities': 'Person.activityField',
    'esc': 'about',
    'isced-1997': 'Service.audience',
    'licenses': 'Service.license/Policy.license',
    'organizations': 'Organization.additionalType',
    'persons': 'Person.additionalType',
    'policies': 'Policy.scope',
    'policyTypes': 'Policy.additionalType',
    'projects': 'Action.additionalType',
    'sectors': 'primarySector/secondarySector',
    'services': 'Service.additionalType'
}

with open ('concepts.csv', 'w') as output_handle:
    fieldnames = ('Feld', '@id', 'en', 'de', 'pt')
    writer = DictWriter(output_handle, fieldnames=fieldnames)
    writer.writeheader()

    for filename, description in filename_mapping.items():
        with open(f"../oerworldmap/ui/src/json/{filename}.json") as handle:
            data = json_load(handle)
            for element in data['hasTopConcept']:
                translations = {value['@language']: value['@value'] for value in element['name']}
                writer.writerow({'Feld': description,
                                 'ID': element['@id'],
                                 'en': translations['en'],
                                 'de': translations['de'],
                                 'pt': translations.get('pt', '')})
