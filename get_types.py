#!/usr/bin/python3

import json
import pprint


F_MAP = {'@Service': '../oerworldmap/ui/src/json/services.json',
         '@Publication': '../oerworldmap/ui/src/json/publications.json',
         'primarySector': '../oerworldmap/ui/src/json/sectors.json',
         'projects': '../oerworldmap/ui/src/json/projects.json',
         'policy': '../oerworldmap/ui/src/json/policyTypes.json',
         'organizations': '../oerworldmap/ui/src/json/organizations.json',
         'persons': '../oerworldmap/ui/src/json/persons.json',
         }

TYPES_MAP = {}

for key, fname in F_MAP.items():
    type_data = json.load(open(fname))
    for concept in type_data['hasTopConcept']:
        name_en = None
        for name in concept['name']:
            if name["@language"] == "en":
                name_en = name['@value']
        TYPES_MAP[name_en] = concept['@id']


if __name__ == '__main__':
    pprint.pprint(TYPES_MAP, width=230)
