from json import load as json_load
from urllib.parse import urlparse

FALLBACK_GENERIC_ABOUT = json_load(open('./conversion_helpers/default_about.json'))
FALLBACK_CROSS_SECTOR = [{'@id': 'https://oerworldmap.org/assets/json/sectors.json#general', '@type': 'Concept', 'name': {'de': 'Sektorenübergreifend', 'en': 'Cross-sector'}}],
RESOURCE_TO_LANG = {
    'urn:uuid:2c19d52b-0ae5-4eb2-87a3-e7fde8f81f27': ['en'],
    'urn:uuid:f0684bac-9f2c-4479-b8bb-2b278717488e': ['de'],
    'urn:uuid:fe1e787e-7c51-40a2-848c-b6c317b02867': ['nl'],
    'urn:uuid:a84dc81d-6350-4db7-abed-b23b6262b3bd': ['en'],
}
COUNTRY_TO_LANG = {
    'US': 'en',
    'NZ': 'en',
    'GB': 'en',
    'DE': 'de',
    'AT': 'de',
    'AU': 'en',
    'ES': 'es',
    'CA': 'en',
    'FR': 'fr',
    'TR': 'tr',
}
TLD_TO_LANG = {
    'do': 'es',
    'es': 'es',
    'co': 'es',
    'bo': 'es',
    'cl': 'es',
    'uy': 'es',
    'cu': 'es',
    'at': 'de',
    'de': 'de',
    'ch': 'de',
    'gov': 'en',
    'ie': 'en',
    'uk': 'en',
    'eu': 'en',
    'ca': 'en',
    'in': 'hi',
    'id': 'ms',
    'br': 'pt',
    'fr': 'fr',
    'dk': 'da',
    'jetzt': 'de',
    'schule': 'de',
    'mx': 'es',
    'se': 'se',
    'pe': 'es',
    'nl': 'nl',
    'lk': 'si',
    'au': 'en',
    'za': 'en',
    'education': 'en',
    'ua': 'ua',
    'directory': 'en',
    'fj': 'fj',
    'tr': 'tr',
}


def convert_service(resource_id, resource, get_resource):
    if 'location' not in resource or not resource['location'] or not resource['location'][0]:
        if 'provider' in resource:
            resource['location'] = [{'address': get_resource(resource['provider'][0]['@id'])['about']['location'][0]['address']}]
        elif 'member' in resource and len(resource['member']) == 1:
            resource['location'] = [{'address': get_resource(resource['member'][0]['@id'])['about']['location'][0]['address']}]
    if 'serviceUrl' in resource['availableChannel'][0]:
        resource['url'] = resource['availableChannel'][0]['serviceUrl']
    if 'availableLanguage' in resource['availableChannel'][0]:
        resource['inLanguage'] = resource['availableChannel'][0]['availableLanguage']
        if resource['inLanguage'][0] == 'por':  # 2 entries have an invalid language
            resource['inLanguage'][0] = 'pt'
        if 'in' in resource['inLanguage']:
            resource['inLanguage'][resource['inLanguage'].index('in')] = 'hi'
        if 'pt-br' in resource['inLanguage']:
            resource['inLanguage'][resource['inLanguage'].index('pt-br')] = 'pt'
    elif resource_id in RESOURCE_TO_LANG:
            resource['inLanguage'] = RESOURCE_TO_LANG[resource_id]
    else:
        tld = urlparse(resource['url']).hostname.split('.')[-1]
        if tld in TLD_TO_LANG:
            resource['inLanguage'] = [TLD_TO_LANG[tld]]
        elif tld in ('org', 'com', 'net', 'edu', 'online', 'is', 'la', 'ninja', 'io', 'academy', 'ng', 'it', 'info'):
            if resource['location'][0] and resource['location'][0]['address']['addressCountry'] in COUNTRY_TO_LANG:
                resource['inLanguage'] = [COUNTRY_TO_LANG[resource['location'][0]['address']['addressCountry']]]
            else:
                resource['inLanguage'] = ['en']
    if 'documentation' in resource['availableChannel'][0]:
        resource['apiDocumentation'] = resource['availableChannel'][0]['documentation']
    del resource['availableChannel']
    if 'about' not in resource:
        resource["about"] = FALLBACK_GENERIC_ABOUT
    if 'primarySector' not in resource:
        resource['primarySector'] = FALLBACK_CROSS_SECTOR
    if 'description' not in resource:
            resource['description'] = {'en': 'Missing description'}
    if 'location' not in resource or not resource['location'][0]:
        if resource_id == 'urn:uuid:7b9fff43-21c7-4afc-a3c1-5d67540f6697':
            resource['location'] = [{'address': {'addressCountry': 'CA'}}]
        elif resource_id in ('urn:uuid:3e9a2931-eb3c-41bb-aa4e-ccc79a728b4d', 'urn:uuid:01910221-e00f-4dfb-97a3-8afcbacebe89'):
            resource['location'] = [{'address': {'addressCountry': 'US'}}]
        elif resource_id in ('urn:uuid:8519f191-79ed-49c7-8c84-fd9afdb65bb5', 'urn:uuid:c4ecc8ce-7f00-11e5-a636-c48e8ff4ea31'):
            resource['location'] = [{'address': {'addressCountry': 'ES'}}]
    else:
        try:
            resource['location'][0]['address']['postalCode'] = str(resource['location'][0]['address']['postalCode'])
        except KeyError:
            pass
    if resource.get('startDate') == '20120101':
        resource['startDate'] = '2012-01-01'
    return resource
