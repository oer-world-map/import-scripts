from convert_display_name import FALLBACK_CROSS_SECTOR

def fix_datetimes(resource: dict) -> (dict, bool):
    changed = False
    for date_field in ('startDate', 'endDate', 'startTime', 'endTime', 'datePublished'):
        if date_field in resource:
            if len(resource[date_field]) == 4:  # YYYY
                resource[date_field] += '-01-01'
                changed = True
            elif len(resource[date_field]) == 7:  # YYYY-MM
                resource[date_field] += '-01'
                changed = True
            elif len(resource[date_field]) == 8:  # YYYYMMDD
                resource[date_field] = f'{resource[date_field][:4]}-{resource[date_field][4:6]}-{resource[date_field][6:8]}'
                changed = True
            elif date_field in ('datePublished', 'startDate', 'endDate') and 'T' in resource[date_field]:  # remove time if only date is allowed
                resource[date_field] = resource[date_field][:10]
                changed = True
    return resource, changed

def convert_schema_date_time(resource_id: int, resource_type: str, resource: dict) -> (dict, bool):
    changed = False
    if resource_type == 'Action' and 'startTime' in resource:
        resource['startDate'] = resource['startTime'].split('T')[0]
        changed = True
        del resource['startTime']
    if resource_type == 'Action' and 'endTime' in resource:
        resource['endDate'] = resource['endTime'].split('T')[0]
        changed = True
        del resource['endTime']
    if resource_type == 'Event' and 'startDate' in resource:
        resource['startTime'] = resource['startDate']
        changed = True
        del resource['startDate']
    if resource_type == 'Event' and 'endDate' in resource:
        resource['endTime'] = resource['endDate']
        changed = True
        del resource['endDate']
    resource, changed_datetime = fix_datetimes(resource)
    changed = changed or changed_datetime
    if resource_id == 'urn:uuid:f15bfd3f-1452-4cec-b30c-621d429ee3d7' and resource.get('endDate') == '2922-09-01':
        resource['endDate'] = '2022-09-01'
    if resource_id == 'urn:uuid:3eff884e-879f-43de-93db-d2af064a2b25' and resource.get('endTime') == '2917-06-14':
        resource['endTime'] = '2017-06-14'
    if resource_id in ('urn:uuid:4b00515b-5e9a-4549-ab9e-b29e569ed972', 'urn:uuid:12c8928e-dbbf-418c-883e-6bf85d843aee', 'urn:uuid:5e13de51-72c0-4f6b-a484-309436aa56bd', 'urn:uuid:a494613a-db20-4eca-bfd2-233af69c8bc4', 'urn:uuid:5c394476-d19c-4472-a619-e4085b41ec06', 'urn:uuid:59e68aa9-fd58-4c7e-bcb5-75fd5a553d98') and 'isBasisFor' in resource:
        del resource['isBasisFor']
    if resource_id in ('urn:uuid:4b00515b-5e9a-4549-ab9e-b29e569ed972', 'urn:uuid:bde9a6b3-ebf4-4c9a-a517-b6ad1ee5d35e') and isinstance(resource.get('isPartOf', {}), list):
        resource['isPartOf'] = resource['isPartOf'][-1]
    if 'https://oerworldmap.org/assets/json/vocab.json#displayName' in resource:
        del resource['https://oerworldmap.org/assets/json/vocab.json#displayName']
    if 'location' in resource and len(resource['location']) == 1 and not resource['location'][0]:  # empty entry, e.g. urn:uuid:da6d890e-2faa-11e5-ba96-001999ac7927
        del resource['location']
    if resource_id in ('urn:uuid:5314276e-c2d1-4b9b-afbb-0cc23bd20c86', 'urn:uuid:3f11cdbd-a74a-4aa4-83b3-b4148bfdbe82', 'urn:uuid:efed6ca2-b228-480f-be03-090a19de7b42', 'urn:uuid:12f252b3-995d-4ed9-b7e5-8e43039981b8', 'urn:uuid:62eae81e-97b8-4434-892f-18d31925469e', 'urn:uuid:98e2fe73-ad19-4b02-b88a-e18bd6ed7a07') and 'provider' in resource:
        del resource['provider']
    try:  # e.g. urn:uuid:e970ed2b-889d-4755-a69c-dd85fcfe88c4
        resource['location'][0]['address']['postalCode'] = str(int(resource['location'][0]['address']['postalCode']))
    except (KeyError, ValueError):
        pass
    if resource_id == 'urn:uuid:5d695f8a-bc88-4417-a754-ce868ecbf1b0' and len(resource['primarySector']) == 2:
        resource['primarySector'] = resource['primarySector'][0]
    return resource, changed
