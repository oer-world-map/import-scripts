#!/usr/bin/python3

import requests
from json import dumps as json_dumps
from argparse import ArgumentParser
from pprint import pprint as pprint_orig
from requests import Session
from sys import exit, stderr
from difflib import ndiff
from copy import deepcopy
from typing import Callable
from convert_service_available_channel import convert_service
from convert_display_name import convert_display_name
from convert_event_alternateName import convert_event_alternateName
from convert_schema_date_time import convert_schema_date_time, fix_datetimes

VERBOSE: bool = False
RED: Callable[[str], str] = lambda text: f"\u001b[31m{text}\033\u001b[0m"
GREEN: Callable[[str], str] = lambda text: f"\u001b[32m{text}\033\u001b[0m"
IGNORE_ERRORS = False


def dict_diff(old: dict, new: dict) -> str:
    # derived from https://stackoverflow.com/a/74336612/2851664 CC BY-SA 4.0
    lines = ndiff(json_dumps(old, indent=4, sort_keys=True).splitlines(keepends=True),
                  json_dumps(new, indent=4, sort_keys=True).splitlines(keepends=True))

    result = ""
    for line in lines:
        line = line.rstrip()
        if line.startswith("+"):
            result += GREEN(line) + "\n"
        elif line.startswith("-"):
            result += RED(line) + "\n"
        elif line.startswith("?"):
            continue
    return result


session = Session()
session.headers.update({
    'Accept': 'application/json',
    })


def get_recursive(dict_object: dict, field: str):
    fieldnames = field.split('.')
    result = dict_object
    for fieldname in fieldnames:
        if isinstance(dict_object, list) and len(dict_object) == 1:
            dict_object = dict_object[0]
        dict_object = dict_object.get(fieldname, {})
    return dict_object


def pprint(*args, **kwargs):
    pprint_orig(*args, width=400, **kwargs)

def get_resource(rid: str):
    response = session.get(f'{args.hostname}/resource/{rid}')
    response.raise_for_status()
    return response.json()

def post_resource(resource_id: str, resource: dict):
    # delete all disallowed fields
    resource_filtered = {key: value for key, value in resource.items() if key in schema[resource['@type']]['properties']}
    response = session.post(f'{args.hostname}/resource/{resource_id}', json=resource_filtered)
    if IGNORE_ERRORS and not response.ok:
        print('Posting data failed. ', end='', file=stderr)
    if VERBOSE:
        print('Request body:', file=stderr)
        pprint(resource, stream=stderr)
    if not response.ok or VERBOSE:
        print(f'Status code: {response.status_code} Response body:', file=stderr)
        try:
            json_output = response.json()
        except Exception:
            print(response.text, file=stderr)
        else:
            if response.status_code == 400:
                json_output = [item for item in json_output if item['level'] == 'error']
            pprint(json_output, stream=stderr)
    if IGNORE_ERRORS and not response.ok:
        return
    response.raise_for_status()

def delete_resource(resource_id: str):
    response = session.delete(f'{args.hostname}/resource/{resource_id}')
    response.raise_for_status()


parser = ArgumentParser(
                    prog='Search and Editor',
                    description='Searches data in OER World Map')
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-D', '--diff', action='store_true')
parser.add_argument('-n', '--dry-run', action='store_true')
parser.add_argument('--step', action='store_true')

parser.add_argument('-H', '--hostname', default='http://localhost:8080')
parser.add_argument('--elastic-hostname', help='If not set, default is Hostname + "/elastic"')
parser.add_argument('--elastic-index', default='oerworldmap')
parser.add_argument('-C', '--cookie', help='Authentication session cookie ("mod_auth_openidc_session=...")')

parser.add_argument('-i', '--ids', nargs='*', help='Resource IDs to process (instead of search)')
parser.add_argument('-t', '--type', nargs='*')
parser.add_argument('-e', '--exists', nargs='*')
parser.add_argument('-E', '--not-exists', nargs='*')
parser.add_argument('-S', '--show-search', action='store_true', help="Show the full resource per search result")
parser.add_argument('-I', '--show-ids', action='store_true', help="Show the resource ID for the search result. Useful for the --exists option without --show-search")
parser.add_argument('--larger-than', type=int, help="Only show fields with more than the given list elements. Only works with --exists and --show-search")
parser.add_argument('-l', '--limit', default=10000, type=int)
parser.add_argument('--all', action='store_true', help='Iterate over all resources (fetched from backend directly)')

parser.add_argument('--delete-field')
parser.add_argument('--delete-resource', action='store_true')
parser.add_argument('--convert-service-available-channel', action='store_true')
parser.add_argument('--convert-display-name', action='store_true')
parser.add_argument('--convert-event-alternateName', action='store_true')
parser.add_argument('--convert-schema-date-time', action='store_true')
parser.add_argument('--fix-datetimes', action='store_true')
parser.add_argument('--check-availability', action='store_true')

parser.add_argument('--show-links', '-L', action='store_true')
parser.add_argument('--ignore-errors', action='store_true')
args = parser.parse_args()

if args.cookie:
    session.headers.update({'Cookie': args.cookie})
VERBOSE = args.verbose

if not args.elastic_hostname:
    args.elastic_hostname = args.hostname + '/elastic'
IGNORE_ERRORS = args.ignore_errors

if args.type or args.exists or args.not_exists:
    print(args.type)
    query = {
      "query": {
        "bool": {
          "must": [],
          "must_not": [],
        }
      },
      "size": args.limit,
      "_source": {
        "includes": [
          "*"
        ]
      }
    }
    if args.type:
        query['query']['bool']['must'].append(
            {
              "terms": {
                "about.@type": args.type
              }
            })
    if args.exists:
        query['query']['bool']['must'].extend([
            {
              "exists": {
                "field": field
              }
            }] for field in args.exists)
    if args.not_exists:
        query['query']['bool']['must_not'].append(
                    [{
                        "exists": {
                            "field": field
                        }
                    } for field in args.not_exists])

    q = requests.post(f'{args.elastic_hostname}/{args.elastic_index}/_msearch?',
                      headers={'Accept': 'application/json',
                               'content-type': 'application/x-ndjson'},
                      data='{"preference":"my_search"}\n%s\n' % json_dumps(query)
                      )

    r = q.json()
    print('Responses:', len(r['responses']))
    hits = r['responses'][0]['hits']['hits']
    print('Hits:', len(hits))
    resources = list(map(lambda x: x['_source'], hits))
    resource_ids = list(map(lambda x: x['_source']['about']['@id'], hits))
elif args.ids:
    resource_ids = args.ids
    print(f'{len(resource_ids)} IDs given')
elif args.all:
    q = requests.get(f'{args.hostname}/label', headers={'Accept': 'application/json'})
    r = q.json()
    resource_ids = [resource['uri']['value'] for resource in r['results']['bindings'] if resource['uri']['value'].startswith('urn:uuid')]
    print(f'Processing all {len(resource_ids)} resources')
else:
    exit('Neither IDs not search given. Exiting')

schema = requests.get(f'{args.hostname}/assets/json/schema.json').json()['definitions']

for i, resource_id in enumerate(resource_ids):
    if args.show_links and not args.show_search:
        print(f'{args.hostname}/resource/{resource_id}')
    if args.show_ids:
        print(resource_id)
    if args.delete_field:
        print(f'Update Resource {resource_id} ({i}/{len(resource_ids)})')
        resource = get_resource(resource_id)['about']
        old_resource = deepcopy(resource)
        if f'https://oerworldmap.org/assets/json/vocab.json#{args.delete_field[6:]}' in resource:
            del resource[f'https://oerworldmap.org/assets/json/vocab.json#{args.delete_field[6:]}']
        else:
            del resource[args.delete_field[6:]]
        if args.verbose or args.diff:
            print(dict_diff(old_resource, resource))
        if args.step:
            answer = input('Continue? (y/N) ').lower().strip()
            if answer != 'y':
                break
        if not args.dry_run:
            post_resource(resource_id, resource)
    elif args.delete_resource:
        print(f'Delete Resource {resource_id} ({i}/{len(resource_ids)})')
        if not args.dry_run:
            delete_resource(resource_id)
    elif args.convert_service_available_channel:
        assert args.type == 'Service'
        print(f'Update Resource {resource_id} ({i}/{len(resource_ids)})')
        resource = get_resource(resource_id)['about']
        if 'availableChannel' not in resource:
            print(f'Skipping, nothing to convert.')
            continue
        old_resource = deepcopy(resource)
        resource = convert_service(resource_id, old_resource, get_resource)
        if args.verbose or args.diff:
            print(dict_diff(old_resource, resource))
        if not args.dry_run:
            post_resource(resource_id, resource)
    elif args.convert_display_name:
        print(f'Update Resource {resource_id} ({i}/{len(resource_ids)})')
        resource = get_resource(resource_id)['about']
        if 'displayName' not in resource:
            print(f'Skipping, nothing to convert.')
            continue
        old_resource = deepcopy(resource)
        resource = convert_display_name(resource_id, old_resource, get_resource)
        if args.verbose or args.diff:
            print(dict_diff(old_resource, resource))
        if not args.dry_run:
            post_resource(resource_id, resource)
    elif args.convert_event_alternateName:
        print(f'Update Resource {resource_id} ({i}/{len(resource_ids)})')
        resource = get_resource(resource_id)['about']
        if 'alternateName' not in resource:
            print(f'Skipping, nothing to convert.')
            continue
        old_resource = deepcopy(resource)
        resource = convert_event_alternateName(resource_id, resource, get_resource)
        if args.verbose or args.diff:
            print(dict_diff(old_resource, resource))
        if not args.dry_run:
            post_resource(resource_id, resource)
    elif args.check_availability:
        print(resource_id, resources[i]['about']['url'], sep=',', end=',')
        url = resources[i]['about']['url']
        try:
            response = requests.get(url, timeout=10)
        except requests.RequestException as exc:
            print('%r' % exc)
        else:
            print(response.status_code)
    elif args.convert_schema_date_time:
        # Action.startTime -> Action.startDate
        # Action.endTime -> Action.endDate
        # Event.startDate -> Event.startTime
        # Event.endDate -> Event.endTime
        # editor.py -t Action -e about.startTime --convert-schema-time-to-date ...
        print(f'Update Resource {resource_id} ({i}/{len(resource_ids)})')
        resource = get_resource(resource_id)['about']
        resource_type = resource['@type']
        if resource_type not in ('Action', 'Event'):
            continue
        old_resource = deepcopy(resource)
        resource, changed = convert_schema_date_time(resource_id, resource_type, resource)
        if not changed:
            print('No changes, skipping')
            continue
        if args.verbose or args.diff:
            print(dict_diff(old_resource, resource))
        if changed and not args.dry_run:
            post_resource(resource_id, resource)
    elif args.fix_datetimes:
        print(f'Update Resource {resource_id} ({i}/{len(resource_ids)})')
        resource = get_resource(resource_id)['about']
        resource_type = resource['@type']
        old_resource = deepcopy(resource)
        resource, changed = fix_datetimes(resource)
        if not changed:
            print('No changes, skipping')
            continue
        if args.verbose or args.diff:
            print(dict_diff(old_resource, resource))
        if changed and not args.dry_run:
            post_resource(resource_id, resource)
    if args.show_search:
        resource = get_resource(resource_id)
        show_data = {}
        if args.exists:
            for field in args.exists:
                field_data = get_recursive(resource, field)
                if args.larger_than and len(field_data) <= args.larger_than:
                    continue
                show_data[field] = field_data
        else:
            show_data = resource['about']
        if show_data:
            if args.show_links:
                print(f'{args.hostname}/resource/{resource_id}')
            print(json_dumps(show_data))
    if args.step and not args.delete_field:  # also do stepping for showing and deleting data
        answer = input('Continue? (y/N) ').lower().strip()
        if answer != 'y':
            break
