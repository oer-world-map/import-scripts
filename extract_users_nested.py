#!/usr/bin/python3

from datetime import datetime
from json import dump as json_dump, load as json_load
from sys import stderr
from glob import glob
from requests import post
from pprint import pprint

DATA = {
  "realm": "oerworldmap",
  "users": []
}

already_imported_ids = set()
already_imported_emails = set()
for usersfile in glob('../oerworldmap/data/keycloak_data/oerworldmap-users-*.json'):
    already_imported_ids |= {x['id'] for x in json_load(open(usersfile))['users']}
    already_imported_emails |= {x['email'] for x in json_load(open(usersfile))['users']}

print(f'Assuming {len(already_imported_ids)} as already imported.')
overlap = 0

def generate_user(user_id: str, name: str, email: str = None) -> dict:
    return {
        "id": str(user_id)[9:],
        "createdTimestamp": int(datetime.now().timestamp()*1000),
        "username": email,
        "enabled": True,
        "totp": False,
        # "emailVerified": True,
        "firstName": ' '.join(name.split(' ')[:-1]).strip(),
        "lastName": name.split(' ')[-1].strip(),
        "email": email,
        "attributes": {
            "profile_id": [str(user_id)]
        },
        "credentials": [],
        "disableableCredentialTypes": [],
        "requiredActions": ["VERIFY_EMAIL", "UPDATE_PASSWORD"],
        "realmRoles": ["default-roles-oerworldmap"],
        "notBefore": 0,
        "groups": []
    }


all_emails = set()

"""
{"preference":"q"}
{"query":{"bool":{"must":[{"bool":{"must":[{"bool":{"boost":1,"minimum_should_match":1,"should":[{"term":{"about.@type":"Person"}}]}}]}}]}},"size":10000,"_source":{"includes":["*"],"excludes":[]}}
"""
response = post('https://oerworldmap.org/elastic/oerworldmap/_msearch?',
                headers={'Accept': 'application/json',
                         'Origin': 'https://oerworldmap.org',
                         'Referer': 'https://oerworldmap.org/resource/?filter.about.@type=%22Person%22&size=20',
                         'content-type': 'application/x-ndjson'},
                data='{"preference":"q"}\n{"query":{"bool":{"must":[{"bool":{"must":[{"bool":{"boost":1,"minimum_should_match":1,"should":[{"term":{"about.@type":"Person"}}]}}]}}]}},"size":10000,"_source":{"includes":["*"],"excludes":[]}}\n')

entries = {}
for hit in response.json()['responses'][0]['hits']['hits']:
    try:
        entry_type = hit['_source']['feature']['properties']['@type']
    except KeyError:
        continue
    if entry_type != 'Person':
        continue
    entry_id = hit['_source']['feature']['properties']['@id']
    if entry_id[9:] in already_imported_ids:
        overlap += 1
        continue
    entries[entry_id] = hit['_source']
    try:
        names = hit['_source']['feature']['properties']['name']
    except KeyError:
        pprint(hit['_source'], width=200)
        break
    if 'en' in names:
        en_name = names['en']
    else:
        en_name = names[list(names.keys())[0]]
        #print(f'{entry_id} has multiple names, but none is English: {names!r}. Using the first one.', file=stderr)
    try:
        email = hit['_source']['about']['email']
    except KeyError:
        try:
            email = hit['_source']['feature']['properties']['about']['email']
        except KeyError:
            #print(f'{entry_id} ({en_name!r}) has no email address. Skipping this entry.')
            continue
    if email in all_emails:
        #print(f'{entry_id} has a duplicate email address: {email}. Skipping this entry.')
        continue
    if email in already_imported_emails:
        print(f'{entry_id} was already imported: {email}. Skipping this entry.')
        continue
    all_emails.add(email)
    DATA['users'].append(generate_user(user_id=entry_id, name=en_name, email=email))

with open('keycloak_users_nested.json', 'w') as output:
    json_dump(obj=DATA, fp=output, indent=2, separators=(',', ': '))
print(f"Exported {len(DATA['users'])} user accounts. Overlap was {overlap}")
