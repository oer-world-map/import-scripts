#!/usr/bin/python3

import argparse
import codecs
import json
import requests

from traceback import print_exc
from pprint import pprint as pprint_orig

from typing import Optional


def pprint(*args, **kwargs):
    pprint_orig(*args, width=230, **kwargs)


def submit(payload):
    member_type = payload['@type']
    response = requests.post(f"http://localhost:9200/oerworldmap/{member_type}", json=payload)
    print('Response:', response)
    try:
        try:
            pprint(list(filter(lambda item: item['domain'] != 'syntax', response.json())))
        except (KeyError, TypeError):
            pprint(response.json())
    except requests.exceptions.JSONDecodeError:
        pprint(response.text)
    response.raise_for_status()


def process_index(index_to_process: Optional[int] = None, verbose: bool = False):
    data = json.load(open(
        "/home/intevation/dump/oer-worldmap-dump.json", encoding="utf8"
    ))
    print(index_to_process)
    submit(data['member'][index_to_process])

def iterate(index_to_process: Optional[int] = None, verbose: bool = False):
    data = json.load(open(
        "/home/intevation/dump/oer-worldmap-dump.json", encoding="utf8"
    ))
    for index, member in enumerate(data['member']):
        if verbose:
            print(f"Processing index {index}")
        submit(member)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')
    parser.add_argument('index', type=int, nargs='?')
    parser.add_argument('-verbose', '-v', action='store_true')
    args = parser.parse_args()
    if args.index:
        process_index(args.index, args.verbose)
    else:
        iterate(args.verbose)
